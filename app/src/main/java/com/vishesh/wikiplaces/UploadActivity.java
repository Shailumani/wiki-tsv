package com.vishesh.wikiplaces;

import android.content.Intent;
import android.net.Uri;
import android.opengl.Visibility;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.os.Build;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class UploadActivity extends ActionBarActivity implements OnTaskCompleted {

	private static final int BROWSE_RESULT_CODE = 1;

	Button submitButton, browseButton;
	ListView pageListView;
	String edittoken;
	/**
	 * ATTENTION: This was auto-generated to implement the App Indexing API.
	 * See https://g.co/AppIndexing/AndroidStudio for more information.
	 */
	private GoogleApiClient client;
	private InputStream in_stream;
	private ArrayList<String> titles = new ArrayList<>();
	ArrayAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		pageListView = (ListView) findViewById(R.id.pagelist);
		pageListView.setVisibility(View.GONE);
		adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1, titles);
		pageListView.setAdapter(adapter);

		submitButton = (Button) findViewById(R.id.submitButton);
		submitButton.setVisibility(View.GONE);
		submitButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				BufferedReader r = new BufferedReader(new InputStreamReader(in_stream));
				String line;
				try {
					line = r.readLine();
					while(line != null){
						String title, text;
						String splitArr[] = line.split("\t");
						title = splitArr[0];
						text = splitArr[1];
						new UploadData(UploadActivity.this, title, text).execute("");
						line = r.readLine();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
//					Network.makeEdit(placeName.getText().toString(), edittoken, starttimestamp, latd, latm, lats, latns, longd, longm, longs, longew);

			}
		});

		browseButton = (Button) findViewById(R.id.browse);

		browseButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent browseIntent = new Intent(Intent.ACTION_GET_CONTENT);
				browseIntent.setType("*/*");
				startActivityForResult(browseIntent, BROWSE_RESULT_CODE);
			}
		});

		// ATTENTION: This was auto-generated to implement the App Indexing API.
		// See https://g.co/AppIndexing/AndroidStudio for more information.
		client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
			case BROWSE_RESULT_CODE:
				if (resultCode == RESULT_OK) {
                    try {
                        in_stream =  getContentResolver().openInputStream(data.getData());
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
					pageListView.setVisibility(View.VISIBLE);
					submitButton.setVisibility(View.VISIBLE);
				}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onTaskCompleted(ArrayList<String> searchResult) {
		if(searchResult.get(0).equals("Success")){
			titles.add(searchResult.get(1));
			adapter.notifyDataSetChanged();
		}
		Toast.makeText(this, searchResult.get(0), Toast.LENGTH_LONG).show();
	}

	@Override
	public void onStart() {
		super.onStart();

		// ATTENTION: This was auto-generated to implement the App Indexing API.
		// See https://g.co/AppIndexing/AndroidStudio for more information.
		client.connect();
		Action viewAction = Action.newAction(
				Action.TYPE_VIEW, // TODO: choose an action type.
				"Upload Page", // TODO: Define a title for the content shown.
				// TODO: If you have web page content that matches this app activity's content,
				// make sure this auto-generated web page URL is correct.
				// Otherwise, set the URL to null.
				Uri.parse("http://host/path"),
				// TODO: Make sure this auto-generated app deep link URI is correct.
				Uri.parse("android-app://com.vishesh.wikiplaces/http/host/path")
		);
		AppIndex.AppIndexApi.start(client, viewAction);
	}

	@Override
	public void onStop() {
		super.onStop();

		// ATTENTION: This was auto-generated to implement the App Indexing API.
		// See https://g.co/AppIndexing/AndroidStudio for more information.
		Action viewAction = Action.newAction(
				Action.TYPE_VIEW, // TODO: choose an action type.
				"Upload Page", // TODO: Define a title for the content shown.
				// TODO: If you have web page content that matches this app activity's content,
				// make sure this auto-generated web page URL is correct.
				// Otherwise, set the URL to null.
				Uri.parse("http://host/path"),
				// TODO: Make sure this auto-generated app deep link URI is correct.
				Uri.parse("android-app://com.vishesh.wikiplaces/http/host/path")
		);
		AppIndex.AppIndexApi.end(client, viewAction);
		client.disconnect();
	}

	private class UploadData extends AsyncTask<String, String, String> {

		String resultEdit;
		String title, text;
		private OnTaskCompleted listener;

		public UploadData(OnTaskCompleted listener, String title, String text) {
			this.listener = listener;
			this.title = title;
			this.text = text;
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String responseStr = Network.getEditToken();
			edittoken = responseStr;
			Log.d("EditToken", responseStr);
			resultEdit = Network.makeEdit(title, text, edittoken);
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			ArrayList<String> resultArray = new ArrayList<String>();
			super.onPostExecute(result);
			resultArray.add(0, resultEdit);
			resultArray.add(1, title);
			listener.onTaskCompleted(resultArray);
		}

	}

}
